## Set up http server and curl from them
# Start mininet
sudo mn

# Start http servers for each folder, on each host.
h1 cd files1
h1 python3 -m http.server >& h1_http.log &
h2 cd files2
h2 python3 -m http.server >& h2_http.log &

# Okay, let's nmap these to find their IP and open ports!
h1 nmap h2
h2 nmap h1

# Now let's read the files
h1 curl 10.0.0.2:8000/dont-read-me.txt
h2 curl 10.0.0.1:8000/words-words-words.txt
