## Let's enable UFW and try again. What changes?
# WARNING - THIS WILL CHANGE THE UFW RULES ON YOUR MACHINE
# run this IN A VM to be safe!

# TODO - How to ISOLATE ufw to specific hosts in mininet?

# Redo from below...

h1 ufw enable
h1 ufw reset
h1 ufw status verbose
h2 ufw enable
h2 ufw reset
h2 ufw status verbose

h1 nmap h2
h2 nmap h1

h1 curl 10.0.0.2:8000/dont-read-me.txt
h2 curl 10.0.0.1:8000/words-words-words.txt

# Let's check that we can still read one anothers stuff
h1 curl 10.0.0.2:8000/dont-read-me.txt
h2 curl 10.0.0.1:8000/words-words-words.txt

## Let's block h1 from reading dont-read-me.txt from h2
h1 ufw deny from any to any port 8000
